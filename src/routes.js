import React from 'react'
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import { App, Whoops404 } from './components'
import SearchContacts from './components/containers/SearchContacts'
import AddContact from './components/containers/AddContact'
import ContactList from './components/containers/ContactList'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const routes = (
    <MuiThemeProvider>
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={SearchContacts}/>
            <Route path="add-contact" component={AddContact}/>
            <Route path="list-contacts" component={ContactList}>
                <Route path=":filter" component={ContactList}/>
            </Route>
        <Route path="*" component={Whoops404}/>
        </Route>
    </Router>
   </MuiThemeProvider>
)

export default routes 