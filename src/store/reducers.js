import C from '../constants'
import { combineReducers } from 'redux'

export const page = (state=1, action) => 
  (action.type === C.SET_PAGES) ? 
     parseInt(action.payload) :
     state


export const pageLength = (state=10, action) => 
  (action.type === C.SET_LIST_LENGTH) ? 
     parseInt(action.payload) :
     state

export const contact = (state=null, action) => 
  (action.type === C.SET_CURRENT_CONTACT) ?
    action.payload :
    state

export const searchText = (state=null, action) => 
    (action.type === C.SET_SEARCH_TEXT) ?
      action.payload :
      state




export const contacts= (state=[], action) => {

  switch(action.type) {

    case C.ADD_CONTACT : 
      const isExist = state.some(contact => 
            contact.phone === action.payload.phone    
      )

      return (isExist) ?
         state :
         [
           ...state,
           action.payload
         ]

    case C.REMOVE_CONTACT :

      return state.filter(contact => contact.phone !== action.payload)     

    default:
      return state
  }

}


export default combineReducers({
  contacts,
  contact,
  page,
  searchText ,
  pageLength,
})


