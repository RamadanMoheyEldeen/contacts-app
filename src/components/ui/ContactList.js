import { PropTypes } from 'react'
import { Link } from 'react-router'
import { Table, TableBody, TableHeader,TableHeaderColumn,TableRow,TableRowColumn } from 'material-ui/Table';
import ContentClear from 'material-ui/svg-icons/content/clear';
import Select from 'react-select';
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import style from './style.scss'
import _ from 'lodash';


const ContactList = ({  contacts ,page , pageLength  ,length , onRemoveContact=f=>f , onSetPages=f=>f , onSetListLength=f=>f }) => {
    
        const remove = (item)=>{

            const  sure = confirm(`Sure you want to delete ( ${item.name} ) Contact Data  ?`) 
            if(sure)
              onRemoveContact(item.phone)
        }

        var from=  page === 1 ? 0 : pageLength * page -1 ; 
        var  to =   page === 1 ? pageLength  : (pageLength * page - 1 ) + pageLength;
    
       return (
           <div>  
         
        <div className="search-container" >
            <label>  Per Page: </label> 
                <TextField 
                        type="number"
                        value={pageLength}
                        underlineStyle={{display: 'none'}}
                        id="pageLength"
                        onChange={(e)=> 
                            {
                                if(e.target.value >= 1)
                                       onSetListLength(e.target.value)
                            }
                        }
                    />
                      <div style={{ display: 'flex'}}>
                        <RaisedButton 
                                style={{display: page == 1 ? 'none' : 'block' }}
                                onClick={ ()=>{onSetPages(page -1)}}>
                                 Previous 
                        </RaisedButton>
                        <RaisedButton  > 
                                {page} 
                        </RaisedButton>
                        <RaisedButton 
                                style={{display: page >= length/pageLength ? 'none' : 'block' }}
                                onClick={ ()=>{onSetPages(page +1)}}>
                                Next 
                        </RaisedButton>                                      
                        </div>
        </div>
         <br/>
          <div>
                <Table>
                <TableHeader displaySelectAll={false}  adjustForCheckbox={false} style={{backgroundColor: "#ddd"}}>
                    <TableRow >
                        <TableHeaderColumn  style={{ height: '10px'}}>Name</TableHeaderColumn>
                        <TableHeaderColumn  style={{ height: '10px'}}>Phone</TableHeaderColumn>
                        <TableHeaderColumn  style={{ height: '10px'}}>Email</TableHeaderColumn>
                        <TableHeaderColumn  style={{ height: '10px'}}>Delete</TableHeaderColumn>

                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false} showRowHover={true}  >
                {_.orderBy(contacts, ['name'],['asc']).slice(from , to).map((contact , index )=>{
                    
                    return(
                        <TableRow key={index}>
                        <TableRowColumn>{contact.name}</TableRowColumn>
                        <TableRowColumn >{contact.phone}</TableRowColumn>
                        <TableRowColumn >{contact.address} </TableRowColumn>
                        <TableRowColumn ><ContentClear onClick={()=>{remove(contact)}}/></TableRowColumn>
                        </TableRow>
                    )
                })}

                </TableBody>
             </Table>
             </div>
        </div>
    )
}



export default ContactList