import { Link } from 'react-router'
import SearchIcon from 'react-icons/lib/md/search'
import AddContactIcon from 'react-icons/lib/md/person-add'
import ListContactIcon from 'react-icons/lib/md/contacts'
import '../../stylesheets/Menu.scss'

const Menu = () =>
    <nav className="menu">
        <Link to="/" activeClassName="selected">
            <SearchIcon />
        </Link>
        <Link to="/add-contact" activeClassName="selected">
            <AddContactIcon />
        </Link>
        <Link to="/list-contacts" activeClassName="selected">
            <ListContactIcon />
        </Link>
    </nav>

export default Menu