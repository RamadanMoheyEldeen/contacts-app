import  React  from 'react'
import Autosuggest from 'react-autosuggest';
import { Table, TableBody, TableHeader,TableHeaderColumn,TableRow,TableRowColumn } from 'material-ui/Table';
import style from './style.scss'
import SearchIcon from 'react-icons/lib/md/search'

const  convertToValidPhoneNumber=(number) => {  
    return number = number.replace(/-/g,"").replace(/^(?=[0-9]{11})([0-9]{5})([0-9]{8})([0-9])$/, "$1-$2-$3");
}

class SearchContacts extends React.Component  
{

    constructor(props){
        super(props)
        this.state = this.state || {
            value:  '',
            contacts: [],
            isLoading: false
        };
    }

   

     search(value){
         var myList = this.props.contacts;
         this.setState({ contacts: myList.filter(item=>{   
                console.log(`
                    Name: ${item.name} 
                    Value: ${value}
                    Result: ${_.includes(item.name, value)}`)
                
              if(
                  _.includes(
                        /^\d+$/.test(value) ? convertToValidPhoneNumber(item.phone) :  item.name.toLowerCase(),
                        /^\d+$/.test(value) ? value : value.toLowerCase()
                    )
                
                ) 
                    return item
            })
        })
     }
       
         
    
    
    onContactsFetchRequested({ value }){
        value = value;
        // this.search();
    }

    
    
    onContactsClearRequested(){}
   
     getContactValue(contact){
          
        this.props.onSetContact({name: contact.name, 
                                phone: contact.phone ,
                                address: contact.address
                            })  
                            
        return contact.name 
    }

     renderContactValue(contact){
         return ( <span> {contact.name} <br/> {contact.phone} </span>)
         }



      onChange(event, { newValue }){
         this.props.setSearch({newValue})
         this.setState({value: newValue },()=>{
            this.search(newValue)
         });

      }

    render(){

        const { value, contacts, } = this.state;       
        const searchInputProps = {
            placeholder: "search by name or phone ",
            value,
            onChange: this.onChange.bind(this) 
        };

        return (
          <div >
            <div className="serach-container" >

                        <Autosuggest
                            suggestions={this.state.contacts}
                            onSuggestionsFetchRequested={this.onContactsFetchRequested.bind(this)}
                            onSuggestionsClearRequested={this.onContactsClearRequested.bind(this)}
                            getSuggestionValue={this.getContactValue.bind(this)}
                            renderSuggestion={this.renderContactValue.bind(this)}
                            inputProps={searchInputProps} 
                            className="react-autosuggest"
                            />

                            <SearchIcon  
                                 style={{width: '3%' , height: '3%'}}
                             />
            </div>

        {
               this.props.contact != null && this.props.contact != undefined ? 
                <div style={{marginTop: "2%"}}>
                <h1>Contact Details: </h1>
                <div>
            <Table >
            <TableHeader displaySelectAll={false}  adjustForCheckbox={false} style={{backgroundColor: '#ddd'}} >
                <TableRow >
                    <TableHeaderColumn >Name</TableHeaderColumn>
                    <TableHeaderColumn >Phone</TableHeaderColumn>
                    <TableHeaderColumn >Email</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} showRowHover={true}  >
                    <TableRow >
                    <TableRowColumn>{this.props.contact.name}</TableRowColumn>
                    <TableRowColumn >{this.props.contact.phone}</TableRowColumn>
                    <TableRowColumn >{this.props.contact.address} </TableRowColumn>
                    </TableRow>
            </TableBody>
         </Table>
         </div>
        </div>:
        ''
        }

         </div>
        )
    }
}



export default SearchContacts