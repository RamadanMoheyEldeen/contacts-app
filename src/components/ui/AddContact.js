import { PropTypes } from 'react'
import '../../stylesheets/AddContact.scss'
import TextField from 'material-ui/TextField'

const AddContact = ({ onAddContact=f=>f , router}) => {

    var _name, _phone, _address



    const isValidMail = email =>  
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        .test(email.toLowerCase()) 

    const isValidPhone = phone =>
          /^\d{2}-\d{3}-\d{4}$/.test(phone)
    
    const isValidName = name =>
       /^[a-zA-Z ]+$/.test(name) 
    
    const validate = (name , phone , email) =>
        isValidName(name) && isValidMail(email) && isValidPhone(phone) 

    

  

        const submit = e => {
            e.preventDefault()
            var form = e.target;
            console.log("Name" , _name)
            console.log(`
                    name: ${form.elements['name'].value}
                    phone: ${form.elements['phone'].value}
                    address: ${form.elements['address'].value}
                `
            )

            let isValid = validate(form.elements['name'].value , form.elements['phone'].value ,form.elements['address'].value) 
                isValid ?  onAddContact({
                    name: form.elements['name'].value,
                    phone: form.elements['phone'].value,
                    address: form.elements['address'].value
                }) :  alert("The data you have enterd are not valid") 

                if(!isValid)
                    return
                const  addAnother = confirm('Add another?') 

            if(!addAnother)
                 router.push('list-contacts')

           form.elements['name'].value= ''
           form.elements['phone'].value= ''
           form.elements['address'].value=''
        }

    return (
        <form onSubmit={submit} className="add-contact">

        <div className="form-group">
            <label className="label" >Name: </label>    
           <TextField 
                placeholder="Full Name"
                className="text-field"
                underlineStyle={{display: 'none'}}
                type="name"
                id="name"
                ref={(name)=>{_name=name}}
                required
             />
             <br/>


        </div>


        <div className="form-group">
        <label className="label">Phone:</label>
        <TextField 
                className="text-field"
                placeholder="xx-xxx-xxxx"
                underlineStyle={{display: 'none'}}
                type="phone"
                id="phone"
                ref={(phone)=>{_phone=phone}}
                required
           />
        </div>

        <div className="form-group">
        <label className="label">E-mail:</label>
           <TextField 
                placeholder="xyz@domain.com"
                className="text-field"
                underlineStyle={{display: 'none'}}
                type="address"
                id="address"
                ref={(address)=>{_address=address}}
                required
              />
        </div>

            <button>Add Contact</button>
        </form>
    )
}

AddContact.propTypes = {

}

export default AddContact