import AddContact from '../ui/AddContact'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { addContact } from '../../actions'

const mapStateToProps = (state, props) => 
	({

	})




const mapDispatchToProps = dispatch => 
	({
		onAddContact({name , phone , address}){
			dispatch(
				addContact(name , phone , address)
			)
		}
	})	

const Container = connect(mapStateToProps, mapDispatchToProps)(AddContact)	

export default withRouter(Container)


