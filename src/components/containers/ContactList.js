import ContactList from '../ui/ContactList'
import { connect } from 'react-redux'
import {  removeContact , setPages , setPageLength   } from '../../actions'

const mapStateToProps = (state, props) => 
  ({
      length: state.contacts.length,
      contacts: state.contacts,
      pageLength: state.pageLength,
      page: state.page,
  })

const mapDispatchToProps = disptach => 
  ({
    onRemoveContact(phone) {
      disptach(
        removeContact(phone)
      )
    },
    onSetPages(page) {
      disptach(
        setPages(page)
      )
    },
    onSetListLength(length) {
      disptach(
        setPageLength(length)
      )
    }

  })  

export default connect(mapStateToProps, mapDispatchToProps)(ContactList)  
