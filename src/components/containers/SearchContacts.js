import SearchContacts from '../ui/SearchContacts'
import { connect } from 'react-redux'
import { setSearchText , setContact } from '../../actions';

const mapStateToProps = (state) => {

	return {
			contacts: state.contacts,
			value: state.searchText,
			contact: state.contact
		   }

}


const mapDispatchToProps = dispatch => 
({
	setSearch(txt){
		dispatch(
			setSearchText(txt)
		)	
		
	},
	onSetContact({ name , phone , address }){
		dispatch(
			setContact(name , phone , address)
		)
	}
})	

const Container = connect(mapStateToProps , mapDispatchToProps )(SearchContacts)

export default Container
