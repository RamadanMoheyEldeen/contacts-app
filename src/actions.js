import C from './constants'
import fetch from 'isomorphic-fetch'

export const addContact= (name,phone,address)=>  {

    return {
        type: C.ADD_CONTACT,
        payload: {name,phone,address}
    }

}


export const setContact = (name,phone,address)=>  {

    return {
        type: C.SET_CURRENT_CONTACT,
        payload: {name,phone,address}
    }

}

export const removeContact = (phone) =>{

    return {
        type: C.REMOVE_CONTACT,
        payload: phone
    }

}

export const setSearchText = (text)=>
    ({
        type: C.SET_SEARCH_TEXT,
        payload: text
    })




export const setPageLength =(num) =>
    ({
        type: C.SET_LIST_LENGTH,
        payload: num
    })


export const setPages = (num) => 
    ({
        type: C.SET_PAGES,
        payload: num
    })



